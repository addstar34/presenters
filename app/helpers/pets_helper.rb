module PetsHelper
  def self.display_name(pet)
    "#{pet.name} the #{pet.breed}"
  end
end
