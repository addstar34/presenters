class PetsController < ApplicationController
  before_action :set_pet, only: [:show, :edit, :update, :destroy]

  def index
    # @pet_presenters = PetPresenter.prepare(Pet.all, view_context)
    # @pet_presenters = Pet.all.map { |pet| PetPresenter.new(pet, view_context) }
    @pets_presenter = PetPresenter::Collection.new(Pet.all, view_context)
  end

  def show
    @pet_presenter = PetPresenter.new(Pet.find(params[:id]), view_context)
  end

  def new
    @pet = Pet.new
  end

  def edit
  end

  def create
    @pet = Pet.new(pet_params)

    if @pet.save
      redirect_to @pet, notice: 'Pet was successfully created.'
    else
      render :new
    end
  end

  def update
    if @pet.update(pet_params)
      redirect_to @pet, notice: 'Pet was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @pet.destroy
    redirect_to pets_url, notice: 'Pet was successfully destroyed.'
  end

  private
    def set_pet
      @pet = Pet.find(params[:id])
    end

    def pet_params
      params.require(:pet).permit(:name, :breed, :age)
    end
end
