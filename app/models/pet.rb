class Pet < ApplicationRecord
  # includes
  # includes
  # has_paper_trail
  # acts_as_paranoid

  # before_save
  # after_save

  # belongs_to
  # belongs_to
  # belongs_to
  # belongs_to
  # has_many
  # has_many
  # has_many
  # has_many
  # has_one

  # scope
  # scope

  # validates
  # validates
  # validates

  # extend enumerize
  # enumerize :sometype, in: [:type_1, :type_2]
  # enumerize :other_type, in: [:other_2, :other_2]

  # def domain_logic
  #     ...
  # end

  # def domain_logic
  #     ...
  # end

  # def domain_logic
  #     ...
  # end

  # def view_logic
  #     ...
  # end

  def display_name
    "#{name} the #{breed}"
  end
end
