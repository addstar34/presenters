class PetPresenter < SimpleDelegator
  attr_reader :pet
  attr_accessor :view_context

  def initialize(pet, view_context)
    super(pet)
    @view_context = view_context
  end

  def display_name
    view_context.link_to "#{name} the #{breed}", self
  end


  class Collection
    attr_reader :collection, :view_context

    def initialize(collection, view_context)
      @collection = collection
      @view_context = view_context
    end

    def each
      collection.each do |item|
        yield self.class.parent.new(item, view_context)
      end
    end
  end
end


# def self.each
#   @@pets.each do |pet|
#     yield new(pet, @@view_context)
#   end
# end
#
# def self.prepare(pets, view_context)
#   @@pets = pets
#   @@view_context = view_context
#   self
# end
