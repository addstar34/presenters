module Collection
  class Collection
    attr_reader :collection, :view_context

    def initialize(collection, view_context)
      @collection = collection
      @view_context = view_context
    end

    def each
      collection.each do |item|
        # binding.remote_pry
        yield self.class.parent.new(item, view_context)
      end
    end
  end
end
