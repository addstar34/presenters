# controller

@pet_presenters = Pet.all.map{ |pet| PetPresenter.new(pet) }
@pet_presenters = Pet.all.map{ |pet| PetPresenter.new(pet, view_context) }

# BASIC PRESENTER NO VIEW_CONTEXT

@pet_presenters = Pet.all.map{ |pet| PetPresenter.new(pet) }

class PetPresenter
  attr_reader :pet

  delegate :name, :breed, :to_model, :id, to: :pet

  def initialize(pet)
    @pet = pet
  end
end


# METHOD_MISSING

class PetPresenter
  attr_reader :pet

  def initialize(pet)
    @pet = pet
  end

  def method_missing(method_name, *args, &block)
    pet.send(method_name, *args, &block)
  end

  def respond_to_missing?(method_name, include_private = false)
    pet.respond_to?(method_name, include_private) || super
  end
end


# SIMPLE DELEGATOR

class PetPresenter < SimpleDelegator
  attr_reader :pet

  def initialize(pet)
    super(pet)
  end
end


# INCLUDE VIEW HELPERS

class PetPresenter < SimpleDelegator
  include ActionView::Helpers
  include Rails.application.routes.url_helpers

  attr_reader :pet

  def initialize(pet)
    super(pet)
  end

  def display_name
    link_to "#{name} the #{breed}", self
  end
end

# VIEW_CONTEXT

class PetPresenter < SimpleDelegator
  attr_reader :pet, :view_context

  def initialize(pet, view_context)
    super(pet)
    @view_context = view_context
  end

  def display_name
    view_context.link_to "#{name} the #{breed}", self
  end
end




# HELPERS

module PetsHelper
  def display_name(pet)
    "#{pet.name} the #{pet.breed}"
  end
end



